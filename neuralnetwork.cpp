#include "neuralnetwork.h"
#include <QDebug>
#include <QTime>
#include <cmath>

NeuralNetwork::NeuralNetwork(QObject *parent) : QObject(parent)
{
    layersCount = 3;
    layer1 = 3;
    layer2 = 2;
    layer3 = 1;
}

void NeuralNetwork::initNeurons()
{
    //генерирую слои с нейронами

    QList<Neural*> *layer = new QList<Neural*>();
    //1 слой
    for (int i = 0; i < layer1; i++)
    {
        Neural *neural = new Neural(this);
        layer->append(neural);
    }

    layers.append(layer);

    //2 слой
    layer = new QList<Neural*>();
    for (int i = 0; i < layer2; i++)
    {
        Neural *neural = new Neural(this, layer1);
        layer->append(neural);
    }

    layers.append(layer);

    //3 слой
    layer = new QList<Neural*>();
    for (int i = 0; i < layer3; i++)
    {
        Neural *neural = new Neural(this, layer2);
        layer->append(neural);
    }

    layers.append(layer);

}

void NeuralNetwork::initWeights()
{
    srand(QTime::currentTime().msecsSinceStartOfDay());

    for (int i = 1; i < layers.count(); i++) //итерация по количеству списков
    {
        for (int j = 0; j < layers.at(i)->count(); j++)//итерация по количеству нейронов в списке
        {
            for (int w = 0; w < layers[i-1]->count(); w++) //итерация по количеством нейронов в прошлом слое
            {
                layers.at(i)->at(j)->setWeight(w, (rand() % 101) / 100.0);
//                layers.at(i)->at(j)->setWeight(w, 1); //для тестов
            }
        }
    }
}

float NeuralNetwork::makePredict()
{
    for (int i = 1; i < layers.count(); i++) //итерация по списками
    {
        for (int j = 0; j < layers.at(i)->count(); j++) //итерация по нейронам
        {
            for (int w = 0; w < layers[i-1]->count(); w++) //итерация по нейронам в прошлом слое
            {
//                qDebug() << w << layers.at(i-1)->at(w)->getOutput();
                layers.at(i)->at(j)->setInput(w, layers.at(i-1)->at(w)->getOutput()); //передаю данные с выходов на прошлом уровне в во входы на текущий нейроне
            }
            layers.at(i)->at(j)->calculateOutput();
        }
    }

//    qDebug() << "Output: " << layers.last()->last()->getOutput();
    return layers.last()->last()->getOutput();
}

float NeuralNetwork::train(QList<QList<float>> trainSet, float learningRate)
{
    float error = 0.0; //складываем результат всех ошибок

    for (int trainIndex = 0; trainIndex < trainSet.count(); trainIndex++)
    {
//        qDebug() << "TRAIN INDEX: " << trainIndex;
        float expectedResult = trainSet[trainIndex].last(); //записываю ожидаемый результат
        trainSet[trainIndex].pop_back(); // удаляю последний элемент, т.к. это ожидаемый результат
        this->setInputs(trainSet[trainIndex]);

        //суммирую данные об ошибке
        float result = makePredict();
//        qDebug() << "Ожидаемый результат " << expectedResult << " - " << result;
        error += pow((expectedResult - result), 2);
//        qDebug() << "Ожидаемый результат " << error;

        for (int i = 0; i < layers.last()->count(); i++) // прохожу через каждый нейрон последнего слоя
        {
            Neural *neural = layers.last()->at(i);
            float deltaWeight = layers.last()->at(i)->calculateDeltaWeight(expectedResult); // получаю дельту весов для текущего нейрона
            neural->setError(deltaWeight);

//                qDebug() << "Весов на нейроне: " <<  neural->getWeights().count();
//                qDebug() << "Веса нейрона: " << neural->getWeights();
//                qDebug() << "Входы нейрона" << neural->getInputs();
//                qDebug() << "Выход нейрона" << neural->getOutput();
//                qDebug() << "Дельта весов: " << deltaWeight;
            for (int j = 0; j < neural->getWeights().count(); j++) //прохожу через каждый вес нейрона и перерасчитываю его
            {
                float newWeight = neural->getWeights()[j] - (neural->getInputs()[j] * deltaWeight * learningRate);
//                    qDebug() << "New weight = " << neural->getWeights()[k] << " - (" << neural->getInputs()[k] << " * " << deltaWeight << " * " << learningRate << ")";
//                    qDebug() << "Прошлый вес" << neural->getWeights()[k] << "Новый вес: " << newWeight;
                neural->setWeight(j, newWeight);
            }
        }


        for (int i = 0; i < layers.at(1)->count(); i++) // прохожу через каждый нейрон 2 слоя (скрытого)
        {
            Neural *neural = layers.at(1)->at(i);
            neural->setError(layers.last()->last()->getError() * layers.last()->last()->getWeights()[i]); // получаю дельту весов для текущего нейрона
            float deltaWeight = layers.at(1)->at(i)->calculateDeltaWeight();
//            qDebug() << "New error: " << neural->getError();
            //                qDebug() << "Весов на нейроне: " <<  neural->getWeights().count();
            //                qDebug() << "Веса нейрона: " << neural->getWeights();
            //                qDebug() << "Входы нейрона" << neural->getInputs();
            //                qDebug() << "Выход нейрона" << neural->getOutput();
            //                qDebug() << "Дельта весов: " << deltaWeight;
            for (int j = 0; j < neural->getWeights().count(); j++) //прохожу через каждый вес нейрона и перерасчитываю его
            {
                float newWeight = neural->getWeights()[j] - (neural->getInputs()[j] * deltaWeight * learningRate);
//                                    qDebug() << "New weight = " << neural->getWeights()[j] << " - (" << neural->getInputs()[j] << " * " << deltaWeight << " * " << learningRate << ")";
//                                    qDebug() << "Прошлый вес" << neural->getWeights()[j] << "Новый вес: " << newWeight;
                neural->setWeight(j, newWeight);
            }
        }
    }



//    qDebug() << error << trainSet.count();
    error = error / trainSet.count(); //получаем среднею квадратичную ошибку
    qDebug() << "Ошибка: "<< error * 100 << "%";

    return error;

}

void NeuralNetwork::setInputs(QList<float> inputsList)
{
    for (int i = 0; i < inputsList.count(); i++)
    {
        layers.at(0)->at(i)->setOutput(inputsList[i]); // передаю на выход первого слоя базовые значения, их не нужно передавать на вход, т.к. не надо рассчитывать их
    }

//    for (int i = 0; i < layers.at(0)->count(); i++)
    //        qDebug() << layers.at(0)->at(i)->getOutput();
}

void NeuralNetwork::setWeights(int layerIndex, int neuronIndex, QList<float> dataList)
{
    for (int i = 0 ; i < dataList.count(); i++)
    {
        layers.at(layerIndex)->at(neuronIndex)->setWeight(i, dataList[i]);
    }
}
