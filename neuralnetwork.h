#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include <QObject>
#include "neural.h"

class NeuralNetwork : public QObject
{
    Q_OBJECT
public:
    explicit NeuralNetwork(QObject *parent = nullptr);
    void initNeurons();
    void initWeights();
    void setInputs(QList<float> inputsList);
    void setWeights(int layerIndex, int neuronIndex, QList<float> dataList);

    float makePredict();
    QList<QList <Neural*>* > getLayers(){return layers;}

    float train(QList<QList<float>> trainSet, float learningRate);

private:
    QList<QList <Neural*>* > layers; //список со слоями, внутри каждого слоя список с нейронами
    int layersCount;
    int layer1; // количество нейронов на 1 слое
    int layer2; // количество нейронов на 2 слое
    int layer3; // количество нейронов на 3 слое


signals:

public slots:
};

#endif // NEURALNETWORK_H
