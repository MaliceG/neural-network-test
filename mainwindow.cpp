#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbShowSmth_clicked()
{
     QList<QList<float>> trainSet =
             QList<QList<float>>{
                                QList<float>{0.0, 1.0, 0.0, 1.0},
                                QList<float>{0.0, 1.0, 1.0, 1.0},
                                QList<float>{0.0, 0.0, 0.0, 0.0},
                                QList<float>{0.0, 0.0, 1.0, 0.0},
                                QList<float>{1.0, 1.0, 0.0, 1.0},
                                QList<float>{1.0, 1.0, 1.0, 1.0},
                                QList<float>{1.0, 0.0, 0.0, 0.0},
                                QList<float>{1.0, 0.0, 1.0, 1.0}
        };

    qDebug() << trainSet;
    NeuralNetwork *network = new NeuralNetwork(this);
    network->initNeurons();
    network->initWeights();
//    network->setWeights(2, 0, QList<float>{0.5, 0.52});
//    network->setWeights(1, 0, QList<float>{0.79, 0.44, 0.43});
//    network->setWeights(1, 1, QList<float>{0.85, 0.43, 0.29});
//    network->setInputs(QList<float>{1, 1, 0});
//    network->makePredict();


    for (int i = 1; i < network->getLayers().count(); i++) //прохожу через каждый слой, кроме 0
    {
        QList <Neural*> *layer = network->getLayers().at(i); // прохожу через каждый нейрон на слое
        for (int j = 0; j < layer->count(); j++)
        {
            qDebug() << "-----------------------------";
            qDebug() << "СЛОЙ: " << i << "Нейрон: " << j << "Input: " << layer->at(j)->getInputs();
            qDebug() << "Weight: " << layer->at(j)->getWeights();
            qDebug() << "Output: " << layer->at(j)->getOutput();
        }
    }

    for (int i = 0; i < 5000; i++)
        network->train(trainSet, 0.1);

    network->setInputs(QList<float>{0, 1, 0}); //1
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{0, 1, 1}); //1
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{0, 0, 0}); //0
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{0, 0, 1}); //0
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{1, 1, 0}); //1
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{1, 1, 1}); //1
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{1, 0, 0}); //0
    qDebug() << network->makePredict();
    network->setInputs(QList<float>{1, 0, 1}); //1
    qDebug() << network->makePredict();

    for (int i = 1; i < network->getLayers().count(); i++) //прохожу через каждый слой, кроме 0
    {
        QList <Neural*> *layer = network->getLayers().at(i); // прохожу через каждый нейрон на слое
        for (int j = 0; j < layer->count(); j++)
        {
            qDebug() << "-----------------------------";
            qDebug() << "СЛОЙ: " << i << "Нейрон: " << j << "Input: " << layer->at(j)->getInputs();
            qDebug() << "Weight: " << layer->at(j)->getWeights();
            qDebug() << "Output: " << layer->at(j)->getOutput();
        }
    }
}
