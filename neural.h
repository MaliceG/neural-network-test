#ifndef NEURAL_H
#define NEURAL_H

#include <QObject>

class Neural : public QObject
{
    Q_OBJECT
public:
    explicit Neural(QObject *parent = nullptr, int inputsWeightsCount = 0); //в конструкторе указываем количество входов и весов (оно равно количеству нейронов на прошлом уровне)

    QList<float> getInputs(){return inputs;}
    QList<float> getWeights(){return weights;}
    float getOutput(){return output;}

    void setInput(int index, float number);
    void appendInput(float number);

    void setWeight(int index, float number);
    void appendWeight(float number);

    void setOutput(float number);

    void calculateOutput();

    float sigmoid(float x);
    float sigmoidDx();

    void setExpectedResult(float number);
    float getExpectedResult(){return expectedResult;}

    float calculateError(float expectedValue);
    float calculateDeltaWeight(float expectedValue); //используется для последнего слоя
    float calculateDeltaWeight(); //используется для скрытого слоя, только после метода setError();

    void setError(float err);
    float getError(){return error;}


private:
    QList<float> inputs;
    QList<float> weights;
    float error;
    float output;
    float expectedResult;


signals:

public slots:
};

#endif // NEURAL_H
