#include "neural.h"
#include <cmath>
#include <QDebug>
#define E 2.718

Neural::Neural(QObject *parent, int inputsWeightsCount) : QObject(parent)
{
    for (int i = 0; i < inputsWeightsCount; i++)
    {
        inputs.append(0);
        weights.append(0);
    }

    expectedResult = -1.00;
}

void Neural::setInput(int index, float number)
{
    inputs[index] = number;
}

void Neural::appendInput(float number)
{
    inputs.append(number);
}

void Neural::setWeight(int index, float number)
{
    weights[index] = number;
}

void Neural::appendWeight(float number)
{
    weights.append(number);
}

void Neural::setOutput(float number)
{
    output = number;
}

void Neural::calculateOutput()
{
    float sum = 0.00;
//    qDebug() << "Inputs before calculate: " << inputs;
    for (int i = 0; i < inputs.count(); i++)
    {
//        qDebug() << "Input " << inputs[i] << "*" << "Weight: " << weights[i] << " = " << inputs[i] * weights[i];
        sum += inputs[i] * weights[i];
    }

    output = sigmoid(sum);
}

float Neural::sigmoid(float x)
{
    return 1.00 / (1.00 + pow(E, -x));
}

float Neural::sigmoidDx()
{
    //(сигмоида входных сигналов нейрона) * (1 - сигмоида входных сигналов нейрона)
    //можно скоратить часть и взять результат нейрона, что и есть сигмоида входных сигналов
    return (output) * (1 - output);
}

void Neural::setExpectedResult(float number)
{
    expectedResult = number;
}

float Neural::calculateError(float expectedValue)
{
//    qDebug () << "Error: " << output - expectedValue;
    return output - expectedValue;
}

float Neural::calculateDeltaWeight(float expectedValue)
{
    float deltaWeight = calculateError(expectedValue) * sigmoidDx();
//    qDebug() << "Delta weight = " << calculateError(expectedValue) << " * " << sigmoidDx();
//    qDebug() << "Delta weight: " << QString::number(deltaWeight, 'g', 40);
    return deltaWeight;
}

float Neural::calculateDeltaWeight()
{
    float deltaWeight = error * sigmoidDx();
//    qDebug() << "Delta weight = " << calculateError(expectedValue) << " * " << sigmoidDx();
//    qDebug() << "Delta weight: " << QString::number(deltaWeight, 'g', 40);
    return deltaWeight;
}

void Neural::setError(float err)
{
    error = err;
}
